import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import re
import string

# Télécharger les ressources NLTK nécessaires
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')

lemmatizer = WordNetLemmatizer()

# Fonction de traitement des commentaires avec NLTK
def nltk_preprocess(comment):
    # Suppression des balises HTML
    comment = re.sub(r'<[^>]+>', '', comment)
    # Tokenisation du texte
    tokens = word_tokenize(comment)
    # Conversion en minuscules
    tokens = [token.lower() for token in tokens]
    # Suppression de la ponctuation
    table = str.maketrans('', '', string.punctuation)
    stripped = [token.translate(table) for token in tokens]
    # Suppression des mots vides (stopwords)
    stop_words = set(stopwords.words('english'))
    words = [word for word in stripped if word not in stop_words]
    # Lemmatisation
    lemmatized_words = [lemmatizer.lemmatize(word) for word in words]
    # Retourner les mots nettoyés entourés de guillemets simples
    return ' '.join([f"'{word}'" for word in lemmatized_words])

def main():
    # Spécifiez les chemins des fichiers CSV
    baby_center_path = './babycenter/scrapped_data/baby_center_extraction_2024-05-29.csv'
    reddit_path = './reddit/web_scrapping/reddit/scrapping/data/extraction_2024-05-27.csv'

    # Chargement des fichiers CSV
    baby_center_df = pd.read_csv(baby_center_path)
    reddit_df = pd.read_csv(reddit_path)

    # Suppression des doublons
    baby_center_df.drop_duplicates(inplace=True)
    reddit_df.drop_duplicates(inplace=True)

    # Gestion des valeurs manquantes
    baby_center_df.fillna('', inplace=True)
    reddit_df.fillna('', inplace=True)

    # Appliquer le pré-traitement sur les colonnes de texte des DataFrames
    baby_center_df['Processed Comment'] = baby_center_df['Comment'].apply(nltk_preprocess)
    reddit_df['Processed Comment'] = reddit_df['Comment'].apply(nltk_preprocess)

    # Sauvegarder les données pré-traitées
    baby_center_df.to_csv('./preprocessed_baby_center.csv', index=False)
    reddit_df.to_csv('./preprocessed_reddit.csv', index=False)

    # Afficher les cinq premiers commentaires nettoyés pour vérifier
    print(baby_center_df['Processed Comment'].head())
    print(reddit_df['Processed Comment'].head())

    print("Preprocessing complete. Data saved to 'preprocessed_baby_center.csv' and 'preprocessed_reddit.csv'.")

if __name__ == "__main__":
    main()
