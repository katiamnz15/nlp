# README du scrapping de BabyCenter

Ce dossier comprend un script python qui extrait les détails des discussions et leurs commentaires du site BabyCenter.

## Fonctionnalités

- Sélection des conversations d'intérêt via des URL spécifiques listées dans le script.
- Extraction des informations des publications (titre, auteur, date, contenu, commentaires) en utilisant `requests`, `BeautifulSoup`, et `pandas`.
- Gestion de la pagination pour collecter tous les commentaires d'une conversation spécifique, extraits à l'aide de leur URL.
- Sauvegarde des données extraites dans un fichier CSV situé dans le dossier `scrapped_data`.