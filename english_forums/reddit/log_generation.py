import logging
import os
import datetime


def create_file(path):
    try:
        if not os.path.exists(path):
            with open(path, 'w'):
                pass

    except Exception as e:
        print(f"An error occurred while setting up {path}: {e}")




