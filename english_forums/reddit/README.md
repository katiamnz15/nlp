# README pour le dossier de scraping Reddit

Ce dossier contient des scripts pour extraire des données de discussions sur Reddit.

## Structure du dossier

```
├── reddit/
│   ├── __pycache__/
│   ├── log_generation.py
│   ├── main_scrap.py
│   ├── reddit_scraper.py
│   ├── web_scrapping/
│   │   ├── reddit/
│   │   │   ├── scrapping/
│   │   │   │   ├── data/
│   │   │   │   │   ├── extraction_2024-05-27.csv
│   │   │   │   ├── errors/
│   │   │   │   │   ├── error_2024-05-27.log
│   │   │   │   ├── logs/
│   │   │   │   │   ├── logs_2024-05-27.log
│   ├── README.md
```

## Contenu du dossier

1. **Scripts de scraping** :
    - `log_generation.py`
    - `main_scrap.py`
    - `reddit_scraper.py`

2. **Répertoire de cache** :
    - `__pycache__`

3. **Répertoire de scraping** :
    - `web_scrapping/reddit/scrapping`

4. **Fichiers de données et de logs** :
    - `web_scrapping/reddit/scrapping/data/extraction_2024-05-27.csv`
    - `web_scrapping/reddit/scrapping/errors/error_2024-05-27.log`
    - `web_scrapping/reddit/scrapping/logs/logs_2024-05-27.log`

5. **README.md** :
    - Contient des instructions et des informations supplémentaires sur les scripts.

## Fonctionnalités des scripts

- **log_generation.py** :
    - Génère et gère les fichiers de logs pour enregistrer les erreurs et les événements importants pendant le scraping.

- **main_scrap.py** :
    - Script principal pour lancer le processus de scraping, en utilisant `reddit_scraper.py` pour extraire les données nécessaires.

- **reddit_scraper.py** :
    - Contient les fonctions principales pour accéder aux discussions Reddit, extraire les informations pertinentes et gérer les données récupérées.

## Données extraites

- Les données extraites des discussions Reddit sont sauvegardées dans un fichier CSV :
    - `web_scrapping/reddit/scrapping/data/extraction_2024-05-27.csv`

## Logs et erreurs

- Les événements importants et les erreurs rencontrées pendant le processus de scraping sont enregistrés dans les fichiers suivants :
    - `web_scrapping/reddit/scrapping/errors/error_2024-05-27.log`
    - `web_scrapping/reddit/scrapping/logs/logs_2024-05-27.log`
