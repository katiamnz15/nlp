import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
import seaborn as sns
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Input
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
import datetime

# Charger les données
baby_center_data = pd.read_csv('./preprocessed_baby_center.csv')
reddit_data = pd.read_csv('./preprocessed_reddit.csv')

# Dictionnaires de mots-clés
alcohol_keywords = ['alcohol', 'drinking', 'wine', 'beer', 'liquor', 'vodka', 'whiskey', 'rum', 'champagne', 'cocktail']
smoking_keywords = ['tobacco', 'smoking', 'cigarette', 'nicotine', 'cigar', 'e-cigarette', 'vaping', 'smoke', 'hookah', 'pipe']
bmi_keywords = ['obese', 'BMI','bmi', 'body mass index', 'overweight', 'weight', 'fat', 'diet', 'nutrition', 'calories', 'exercise']
health_problem_keywords = ['problem','problems', 'consequences', 'harm', 'dangerous', 'unhealthy', 'issue', 'illness', 'disease', 'condition', 'adhd','fdas','hyperactivity','eclampsia','preterm','lead','seizure']

# Fonction pour détecter les mots-clés dans les commentaires
def contains_keywords(text, keywords):
    if not isinstance(text, str):
        return False
    return any(keyword in text for keyword in keywords)

# Identifier la colonne contenant les commentaires
comment_column = 'Processed Comment'  # Ajustez selon les colonnes affichées
original_comment_column = 'Comment'  # Colonne avec les commentaires complets

# Convertir les valeurs de la colonne en chaînes de caractères
baby_center_data[comment_column] = baby_center_data[comment_column].astype(str)
reddit_data[comment_column] = reddit_data[comment_column].astype(str)

# Ajouter des colonnes d'indicateurs pour l'alcool, le tabac, l'IMC et les problèmes de santé
baby_center_data['alcohol'] = baby_center_data[comment_column].apply(lambda x: 1 if contains_keywords(x, alcohol_keywords) else 0)
baby_center_data['smoking'] = baby_center_data[comment_column].apply(lambda x: 1 if contains_keywords(x, smoking_keywords) else 0)
baby_center_data['BMI'] = baby_center_data[comment_column].apply(lambda x: 1 if contains_keywords(x, bmi_keywords) else 0)
baby_center_data['child_health'] = baby_center_data[comment_column].apply(lambda x: 1 if contains_keywords(x, health_problem_keywords) else 0)

reddit_data['alcohol'] = reddit_data[comment_column].apply(lambda x: 1 if contains_keywords(x, alcohol_keywords) else 0)
reddit_data['smoking'] = reddit_data[comment_column].apply(lambda x: 1 if contains_keywords(x, smoking_keywords) else 0)
reddit_data['BMI'] = reddit_data[comment_column].apply(lambda x: 1 if contains_keywords(x, bmi_keywords) else 0)
reddit_data['child_health'] = reddit_data[comment_column].apply(lambda x: 1 if contains_keywords(x, health_problem_keywords) else 0)

# Combiner les datasets
data = pd.concat([baby_center_data, reddit_data])

# Sélectionner les features et la cible
X = data[['alcohol', 'smoking', 'BMI']]
y = data['child_health']

# Diviser les données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Vérification de la division des données
print(f'Taille de l\'ensemble d\'entraînement : {X_train.shape[0]}')
print(f'Taille de l\'ensemble de test : {X_test.shape[0]}')

# Standardiser les données
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Construire le modèle de deep learning
model = Sequential()
model.add(Input(shape=(X_train_scaled.shape[1],)))  # Utiliser Input pour spécifier la forme d'entrée
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(1, activation='sigmoid'))  # 'sigmoid' pour une classification binaire

# Compiler le modèle
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# Préparer TensorBoard
log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)

# Entraîner le modèle
early_stopping = EarlyStopping(monitor='val_loss', patience=5)
history = model.fit(X_train_scaled, y_train, validation_split=0.2, epochs=100, batch_size=32, callbacks=[early_stopping, tensorboard_callback])

# Évaluer le modèle
loss, accuracy = model.evaluate(X_test_scaled, y_test)
print(f'Loss: {loss}, Accuracy: {accuracy}')

# Prédictions sur les données de test
y_pred = (model.predict(X_test_scaled) > 0.5).astype("int32")

# Scores supplémentaires
precision = precision_score(y_test, y_pred)
recall = recall_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)
accuracy = accuracy_score(y_test, y_pred)

print("\nScores supplémentaires:")
print(f'Precision: {precision}')
print(f'Recall: {recall}')
print(f'F1-Score: {f1}')
print(f'Accuracy: {accuracy}')

# Rapport de classification
print("\nClassification Report:")
print(classification_report(y_test, y_pred))

# Matrice de confusion
print("\nConfusion Matrix:")
cm = confusion_matrix(y_test, y_pred)
sns.heatmap(cm, annot=True, fmt='d', cmap='Blues')
plt.xlabel('Predicted')
plt.ylabel('True')
plt.show()

# Afficher quelques prédictions sur les commentaires
sample_indices = X_test.index[:50]  # Augmenter le nombre d'échantillons affichés
sample_comments = data.loc[sample_indices, original_comment_column]
sample_predictions = y_pred[:50]

print("\nPrédictions sur quelques commentaires:")
print("Prédits comme 'No Problem':")
for comment, prediction in zip(sample_comments, sample_predictions):
    if prediction[0] == 0:
        print(f"Comment: {comment}")
        print(f"Prediction (0 = No Problem, 1 = Problem): {prediction[0]}")
        print("-" * 50)

print("\nPrédits comme 'Problem':")
for comment, prediction in zip(sample_comments, sample_predictions):
    if prediction[0] == 1:
        print(f"Comment: {comment}")
        print(f"Prediction (0 = No Problem, 1 = Problem): {prediction[0]}")
        print("-" * 50)

