# RAPPORT FINAL DU PROJET

AANANOU Soukaina                                                                                      
ISMAIL Manel 
MENZOU Katia 

M2 DSS 

Ce projet comprend des données extraites, prétraitées et analysées provenant de trois grandes sources d'information : 
- des articles scientifiques (sous-dossier : `/articles`)
- des forums français (sous-dossier : `/fr_forums`) 
- des forums anglais (sous-dossier : `/english_forums`).