import pandas as pd
import requests
from bs4 import BeautifulSoup
import logging

# Configurer les logs pour afficher des informations de debug
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def extract_post_links(url):
    """
    Extrait tous les liens des posts d'un forum à partir d'une URL de page de forum.
    """
    all_post_links = []
    while url:
        logging.info(f"Fetching posts from URL: {url}")  # Afficher une information de log pour chaque URL traitée
        try:
            response = requests.get(url, timeout=10)  # Envoyer une requête GET à l'URL
            response.raise_for_status()  # Lever une exception si la requête a échoué
        except requests.RequestException as e:
            logging.error(f"Error fetching {url}: {e}")  # Loguer une erreur en cas de problème avec la requête
            break

        soup = BeautifulSoup(response.content, 'html.parser')  # Parser le contenu HTML de la page
        
        # Trouver tous les liens dans les cellules de sujet de forum
        posts = soup.find_all('td', class_='forum-subjects')
        for post in posts:
            link = post.find('a', href=True)  # Extraire les liens des posts
            if link and link['href']:
                post_link = link['href']
                if post_link.startswith('/'):
                    post_link = 'https://www.mamanpourlavie.com' + post_link  # Convertir les liens relatifs en liens absolus
                all_post_links.append(post_link)  # Ajouter le lien à la liste

        # Trouver le lien vers la page suivante
        next_page = soup.find('li', class_='next')
        if next_page and next_page.find('a'):
            next_page_link = next_page.find('a').get('href')
            if next_page_link:
                url = 'https://www.mamanpourlavie.com' + next_page_link  # Mettre à jour l'URL pour la page suivante
        else:
            url = None
        
        logging.info(f"Collected {len(all_post_links)} post links so far.")  # Loguer le nombre de liens collectés jusqu'à présent

    return all_post_links

def extract_posts(url):
    """
    Extrait les textes des posts d'un forum à partir d'une URL de page de post.
    """
    all_posts = []
    logging.info(f"Extracting posts from URL: {url}")  # Loguer l'URL du post en cours d'extraction
    try:
        response = requests.get(url, timeout=10)  # Envoyer une requête GET à l'URL du post
        response.raise_for_status()  # Lever une exception si la requête a échoué
    except requests.RequestException as e:
        logging.error(f"Error fetching {url}: {e}")  # Loguer une erreur en cas de problème avec la requête
        return all_posts

    soup = BeautifulSoup(response.content, 'html.parser')  # Parser le contenu HTML de la page de post
    posts = soup.find_all('div', class_='desc thread space-top space')  # Trouver tous les éléments de post
    for post in posts:
        post_text = ' '.join(segment.strip() for segment in post.stripped_strings)  # Extraire et nettoyer le texte du post
        all_posts.append(post_text)  # Ajouter le texte du post à la liste
    
    logging.info(f"Extracted {len(all_posts)} posts from URL: {url}")  # Loguer le nombre de posts extraits
    return all_posts

def save_posts_to_csv(posts_by_url, filename):
    """
    Sauvegarde les posts extraits dans un fichier CSV.
    """
    logging.info(f"Saving posts to CSV file: {filename}")  # Loguer le début de la sauvegarde des données
    all_posts = []
    for url, post_urls in posts_by_url.items():
        for post_url in post_urls:
            post_texts = extract_posts(post_url)  # Extraire les textes des posts pour chaque URL
            for text in post_texts:
                all_posts.append({'URL': post_url, 'Text': text})  # Ajouter chaque post à la liste finale
    df = pd.DataFrame(all_posts)  # Créer un DataFrame à partir de la liste de posts
    df.to_csv(filename, index=False)  # Sauvegarder le DataFrame dans un fichier CSV
    logging.info(f"Data saved to {filename} successfully.")  # Loguer la réussite de la sauvegarde des données

def main(urls):
    """
    Fonction principale pour extraire les posts des URLs fournies.
    """
    all_posts = {}
    for url in urls:
        logging.info(f"Scraping forum page: {url}")  # Loguer l'URL de la page de forum en cours de scraping
        post_links = extract_post_links(url)  # Extraire les liens des posts de la page de forum
        all_posts[url] = post_links  # Associer les liens des posts à l'URL de la page de forum
        logging.info(f"Number of posts links retrieved from {url}: {len(post_links)}")  # Loguer le nombre de liens de posts extraits
    return all_posts

# Liste des URLs des forums à scraper
urls = [
    "https://www.mamanpourlavie.com/forum/forum/on-se-presente",
    "https://www.mamanpourlavie.com/forum/forum/temoignages-de-vie",
    "https://www.mamanpourlavie.com/forum/forum/maman-famille-au-naturel",
    "https://www.mamanpourlavie.com/forum/forum/evolution-de-la-grossesse",
    "https://www.mamanpourlavie.com/forum/forum/alimentation-de-maman",
    "https://www.mamanpourlavie.com/forum/forum/bebe-premature",
    "https://www.mamanpourlavie.com/forum/forum/evolution-de-bebe",
    "https://www.mamanpourlavie.com/forum/forum/sante-et-soins-de-bebe",
    "https://www.mamanpourlavie.com/forum/forum/sante-soins-et-beaute-de-maman",
    "https://www.mamanpourlavie.com/forum/forum/en-forme"
]

if __name__ == "__main__":
    # Extraire les posts de toutes les URLs fournies
    posts_by_url = main(urls)
    # Sauvegarder les posts extraits dans un fichier CSV
    save_posts_to_csv(posts_by_url, 'forum_posts.csv')
    logging.info("Les données ont été sauvegardées dans 'forum_posts.csv'.")