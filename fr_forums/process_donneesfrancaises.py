import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import re
import string

# Télécharger les ressources NLTK nécessaires
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')

lemmatizer = WordNetLemmatizer()

# Fonction de traitement des commentaires avec NLTK
def nltk_preprocess(comment):
    # Suppression des balises HTML
    comment = re.sub(r'<[^>]+>', '', comment)
    # Tokenisation du texte
    tokens = word_tokenize(comment)
    # Conversion en minuscules
    tokens = [token.lower() for token in tokens]
    # Suppression de la ponctuation
    table = str.maketrans('', '', string.punctuation)
    stripped = [token.translate(table) for token in tokens]
    # Suppression des mots vides (stopwords)
    stop_words = set(stopwords.words('french'))
    words = [word for word in stripped if word not in stop_words]
    # Lemmatisation
    lemmatized_words = [lemmatizer.lemmatize(word) for word in words]
    # Retourner les mots nettoyés entourés de guillemets simples
    return ' '.join([f"'{word}'" for word in lemmatized_words])

def main():
    # Spécifiez les chemins des fichiers CSV
    french_data_path = './forum_posts1.csv'

    # Chargement des fichiers CSV
    french_data_df = pd.read_csv(french_data_path, sep=';')

    # Suppression des doublons
    french_data_df.drop_duplicates(inplace=True)

    # Gestion des valeurs manquantes
    french_data_df.fillna('', inplace=True)

    # Appliquer le pré-traitement sur les colonnes de texte des DataFrames
    french_data_df['Processed Comment'] = french_data_df['Text'].apply(nltk_preprocess)

    # Sauvegarder les données pré-traitées
    french_data_df.to_csv('./post_french_preprocessed.csv', index=False)

    # Afficher les cinq premiers commentaires nettoyés pour vérifier
    print(french_data_df['Processed Comment'].head())
    print("Preprocessing complete. Data saved to 'post_french_preprocessed.csv'.")

if __name__ == "__main__":
    main()